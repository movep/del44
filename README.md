The name of game is "Deletion 44" as it can delete 44 same color neighbor stones at most by copy action. 

==Rules==
1. Select a source stone by mouse button clicked.
2. Then, select a target stone that neighbors a source stone (up, down, left, right) by mouse button clicked. 
3. If target stones have more than 3 stones as same color in a row (for right or left neighbor) or in a column (for up or down neighbor), copy the color of source stone to target stones. Otherwise, go to step 1.
4. Delete stones if there are 5 stones or greater have same color in a row or in a column.

==Chinese Readme==

這個遊戲我取名叫Deletion 44，因為使用複製動作(手動)來消除時，一次最多消去44個相鄰的同色石頭。
當不是手動消除時，有一些情況可能超過44個，例如遊戲一開始、或者石頭自動下降許多同色石頭時，
有可能消除超過44個，但是此種情況非常少見。

玩法如下：
1. 用滑鼠選擇一個石頭，當作來源石
2. 接下來，用滑鼠選擇一個來源石旁邊(上、下、左、右)的石頭，當作目標石
3. 如果目標石的行(上下相鄰時)或列(左右相鄰時)，存在同顏色的連續三個以上，就把來源石複製到目標石，否則回到step 1.
4. 當行或列，存在5個或以上，同顏色的石頭，就會消除石頭

算分方式：
1. 剛好消除5個石頭，得5分。這邊的消除個數必需同色相鄰的石頭，如果開局時，一堆5個，另一堆也5個，則是5 + 5 = 10分，以下算分也是同色相鄰的石頭
2. 消除6到9個石頭，得到消除個數的平方分數，例如6個，是6 * 6=36分
3. 消除10到13個石頭，得到消除個數的三次方分數，例如10個，是10 * 10 * 10 = 1000分
4. 消除14到43個石頭，得到消除個數的四次方分數，例如15個，是15 * 15 * 15 * 15 = 50625分
5. 消除44個石頭或以上，得到五次方分數，例如44個，分數是44 * 44 * 44 * 44 * 44 = 164916224分

全部消除?
如果同色的石頭太少，造成難以消除，會啟動全部消除，但此時只消除，不記分。
